/*
  *Nombre archivo: scene.h
  *Autor: Cristian Leonardo Ríos López
  *Fecha de creación: Septiembre 2011
  *Fecha de ultima modificación: Diciembre 2011
  *Descripción: la clase scene permite elaborar el dibujo del tablero y los carros, también permite realizar la animacion de dichos
  *             elementos, mostrando los diferentes estados del tablero para ir desde el tablero inicial al tablero que tiene al
  *             carro A en la meta.
  *Universidad del Valle
*/

#ifndef SCENE_H
#define SCENE_H

#include <QGraphicsScene>
#include "item.h"
#include "car.h"
#include "action.h"

class Scene : public QGraphicsScene
{
    Q_OBJECT

public:
    enum Type {Libre, Obstaculo, AutoDosH, AutoDosV, AutoTresH, AutoTresV};

    Scene(int rowsMax, int columnsMax, QObject *parent = 0);
    ~Scene();
    int carsMany();
    void clearBoard();
    bool containsCar(QString letter, Car::directionValue direction = Car::Ninguna, int size = -1);
    void deleteElement(Item* item);
    void drawGrid();
    char** getBoard();
    QVector<Car> getCars();
    int getColumnMax();
    QString getLetterCarInserted();
    int getRowMax();
    QList<Item*> items_();
    void setBoard(char** board, int rows, int columns);
    void setCars(QVector<Car> cars);
    void setColumnMax(int columnsMany);
    void setLetterCarInserted(const QString letter);
    void setObstacles(QVector<QPoint> obstacles);
    void setRowMax(int rowsMany);
    void setType(Type type);
    void restartGraphics();
    void runAnimation(QVector<Action> movements);
    QList<Item*> selectedItems_();


private:
    char** board;
    int rowMax;
    int columnMax;
    QVector<Car> cars;
    QVector<Item*> carsGraphics;
    QString letterCarInserted;
    Type myType;
    QVector<QPoint> obstacles;
    QVector<Item*> obstaclesGraphics;

    void drawElements();
    Item* findCar(char id);
    QPoint howPositionBoard(QPointF position);
    bool overlapElements(QPoint position);
    Scene::Type typeCar(Car &car);
    void updateBoard(QPoint position, Type type, int action, char letter);
    bool validatePosition(QPoint position);
    QString whichImage(Car::directionValue direction,char letter, int size);

protected:
    void mousePressEvent(QGraphicsSceneMouseEvent *mouseEvent);

signals:
     void itemInserted(Item *item);
     void itemDelete(Item *item);

};

#endif // SCENE_H
