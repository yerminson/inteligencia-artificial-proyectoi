/*
  *Nombre archivo: reader.h
  *Autor: María Andrea Cruz Blandón
  *       Yerminson Doney Gonzalez Muñoz
  *       Cristian Leonardo Ríos López
  *Fecha de creación: Septiembre 2011
  *Fecha de ultima modificación: Diciembre 2011
  *Descripción: La clase Reader permite leer el archivo txt con la configuracion del tablero. esta clase crea
  *             el tablero de la configuracion inicial. tambien instancia el vector de estados de cada carro-
  *             condiciones iniciales de los carros, tamaño posicion y dirección, es lo mismo para los obstaculos.
  *Universidad del Valle
*/

#ifndef READER_H
#define READER_H

#include <QVector>
#include <QPoint>
#include "car.h"
#include <QObject>

class Reader
{
private:
    char **board;
    QVector<Car> states;
    QVector<QPoint> obstacles;
    int widht;
    int height;

    void searchCar(int x, int y,char letter,char** copy);
    void pathClean(int x, int y,Car::directionValue dir,int size,char** copy);
    bool testPosition(int x, int y);



public:
    Reader();
    ~Reader();

    void load(QString rute);
    char** getCopyBoard();
    QVector<Car> getStates();
    QVector<QPoint> getObstacles();
    void createStates();
    int getHeight();
    int getWidth();

};

#endif // READER_H
