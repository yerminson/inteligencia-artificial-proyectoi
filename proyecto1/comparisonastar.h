#ifndef COMPARISONASTAR_H
#define COMPARISONASTAR_H

#include "nodo.h"

class ComparisonAStar
{
public:
    bool operator()(Nodo* nodo1, Nodo* nodo2)
    {
        if(nodo1->getF() >= nodo2->getF())
            return true;
        else
            return false;

    }
};

#endif // COMPARISONASTAR_H
