 /*
  *Nombre archivo: mainwindow.h
  *Autor: Cristian Leonardo Ríos López
  *Fecha creación: 20 Septiembre de 2011
  *Fecha última actualización: 17 Dicciembre de 2011
  *Descripción: La clase MainWindow es la clase principal, permite reunir todos lo elementos
      gráficos que permiten interactuar con el programa, permite cargar estados de búsqueda,
      como tambien la creación dinámica de estos. Teniendo un estado de búsqueda permite
      ejecutar los algoritmos de busqueda y usando otras clases mostrar los resultado y
      las anmaciones de estos.
  *Universidad del Valle
  */

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QtGui/QMainWindow>
#include "scene.h"

QT_BEGIN_NAMESPACE
class QGraphicsView;
class QComboBox;
class QSpinBox;
class QPushButton;
class QToolBox;
class QButtonGroup;
class QWidget;
class QStringList;
class QLabel;
QT_END_NAMESPACE


class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    int buttonisChecked;

    void createToolBox();
    void createActions();
    void createMenus();
    void createToolbars();

    QWidget* createElementCellWidget(const QString &text, const QString &image, const Scene::Type type);
    QWidget* createPanelControl();
    QWidget* createPanelElements();
    QWidget* createWidgetResults();

    QGraphicsView *view;
    Scene *scene;

    QComboBox *algorithmComboBox;
    QComboBox *algorithmTypeComboBox;
    QComboBox *letterComboBox;

    QPushButton *runPushButton;
    QPushButton *restartPushButton;
    QPushButton *clearPushButton;

    QMenu *fileMenu;
    QMenu *itemMenu;
    QMenu *helpMenu;

    QAction *exitAction;
    QAction *importFileAction;
    QAction *exportFileAction;
    QAction *deleteAction;
    QAction *aboutAction;
    QAction *helpAction;

    QToolBar *editToolBar;
    QToolBar *fileToolBar;
    QToolBar *moreToolBar;

    QToolBox *toolBox;

    QButtonGroup *buttonGroup;

    QStringList letterAvailable;

    QLabel *labelResultDepthTree;
    QLabel *labelResultManyNodes;
    QLabel *labelResultTime;
    QLabel *labelResultCost;

 private slots:
    void about();
    void algotithmTypeChange(const int &value);
    void buttonGroupClicked(int id);
    void clearBoard();
    void deleteElement();
    void exportFile();
    void help();
    void importFile();
    void itemDelete(Item* item);
    void itemInserted(Item* item);
    void letterCarChange(const QString &value);
    void restartBoard();
    void runAlgorithm();
};

#endif // MAINWINDOW_H
