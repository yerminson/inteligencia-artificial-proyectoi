Aca tenemos el Readme:

Se esta usando lo sigueinte para el desarrollo

Qt Creator 2.1.0 
Based on Qt 4.7.2

***No agregar la carpeta de compilacion ni el archivo .pro.user a git

Sobre el funcionamiento

- cuando se haga referencia a clic este significará click izquierdo.
- no ejecutará hasta que exista como mínimo un carro en el tablero.
- no ejecutará hasta que exista un carro de longitud 2, hoizontal, identificado con la letra 'A' en el tablero.
- el botón "reiniciar tablero" sirve para restauras las posiciones originales de los elementos del tablero despues de haber corrido un algoritmo, por eso solo estará disponible al terminar una ejecución.
- el boton "limpiar tablero" borra todos los elementos existentes en el tablero.
- se pueden agregar nuevos elementos de dos maneras, la primera consiste en cargar un archivo con la representación del mismo, la segunda aconsiste en crear un tablero de manera dinámica.
- para cargar un tablero de archivo se puede utilizar el boton de la barra de herramientas o la opcion "Importar Tablero" del menú "Archivo".
- si se carga un tablero desde archivo este puede ser editado.
- en tablero actual puede ser guardado en un archivo de texto para su posterior carga.
- la aplicacion tiene dos paneles, el panel de control y el panel de elementos.
- para la construción del tablero dinámico se usa el panel de elementos.
- para agregar un nuevo elemento, se debe dar clic en alguno de los botones del panel de elementos, según se desee. Si el elemento necesita un identificador se usará el que se encuentre seleccionado, este se puede cambiar según la disposicion de identificadores que hayan, posteriormente se da clic en el tablero, el clic se debe de dar en la posicion donde se desea empieza el elemento.
- una vez dado clic en algun botón del panel de elementos, este no se desactivará, permitiendo agregar varios elementos del mismo tipo si se continua dando click en el tablero, para desseleccionarlo se le debe volver a dar clic al botón.
- para eliminar elementos del tablero se debe de seleccionar el o los elementos a eliminar y luego dar clic en la X de la barra de herramientas o en la opción "Eliminar" del menú "Elemento".
- para seleccionar elementos asegurece de que ningún boton del panel de elementos se encuentre presionado, la seleción se realiza dando doble click al elemento. Si se desean seleccioanr varios elementos, mantenga presionada la tecla Ctrl mientras realiza la opeación anteriormente descrita.
- en el tablero solo podrán existir elementos con identificador a excepción de los obstáculos, no se podrán agregar elementos sin identificador. Lo identificadores empiezan en 'A' y terminan en 'I' (****por definir, pueden ser más)
- al momento de agregar un elemento este debe de caber en el tablero, de lo contrario no se podrá agregar.
- al momento de agregar un elemento este no se debe sobreponer a ningún otro elemento, si esto ocurre el elemento no se podrá agregar.  

