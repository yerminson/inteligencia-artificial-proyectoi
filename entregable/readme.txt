El proyecto esta realizado usando como lenguaje C++ y las librerias de Qt 4.7.4 que deben de estar instaladas para
poder compilar y generar un ejecutable o para ejecutar si ya se tiene el ejecutable. No esta presente el ejecutable
por motivos de peso en el archivo

-compilacion sobre linux
   -usando comandos en terminal
      situese en la carpeta codigo y ejecute:
         qmake
         make 
      esto ejenerar un ejecutable llamado proyecto1
      para ejecutar escriba ./proyecto1

   -usando qtCreator(siempre que este esté bien configurado)
      abra con qtCreator el archivo proyecto1.pro
      de click derecho sobre el proyecto y seleccione la opcion run, esto compilará y ejecutará automáticamente,
      pero el ejecutable que queda se puede seguir usando sin tener que volver a compilar.

-compilando sobre windows
   -usando qtCreator
      siga los pasos descritos en la sección anterior.
