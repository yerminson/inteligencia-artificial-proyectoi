/*

* Nombre archivo : item.h
* Nombre de la clase : Item
* Autor: Cristian Leonardo Rios

* Fecha de creación: Octubre del 2011
* Fecha de modificación: Diciembre del 2011
*
* Descripción: La clase Item permite representar graficamente los elementos del ambiente,
* como lo son un carro y un obstaculo, por lo tanto para representar cada uno de estos objetos
* en el plano se tuvieron en cuenta los siguientes atributos  ancho, alto, posicion x inicial,
* posicion y inicial, la letra que describe a este item en caso de ser un carro tendra un valor
* asociado, la posicion en x y la posicion y donde se encuentra el item actualmente. Las posiciones
* iniciales se tienen encuenta en caso de que se necesite volver el item al origen.
*
* Universidad del Valle
*/





#ifndef ITEM_H
#define ITEM_H

#include <QGraphicsPixmapItem>

class Item : public QObject, public QGraphicsPixmapItem
{
private:

    Q_OBJECT
    Q_PROPERTY(QPointF pos READ pos WRITE setPos)

    int width;
    int height;
    int xStart;
    int yStart;
    char letter;
    int x;
    int y;

public:
    Item(const QPixmap pix, int width, int height, int xStart, int yStart, char letter);

    void setWidth(int width);
    void setHeight(int height);
    void setXstart(int xStart);
    void setYstart(int yStart);
    void setLetter(char letter);
    void setX(int x);
    void setY(int y);

    int getWidth();
    int getHeight();
    int getXstart();
    int getYstart();
    char getLetter();
    int getX();
    int getY();
};

#endif // ITEM_H
