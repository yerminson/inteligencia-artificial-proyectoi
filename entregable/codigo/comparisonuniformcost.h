#ifndef COMPARISONUNIFORMCOST_H
#define COMPARISONUNIFORMCOST_H

#include "nodo.h"

class ComparisonUniformCost
{
public:
    bool operator()(Nodo* nodo1, Nodo* nodo2)
    {
        if(nodo1->getG() >= nodo2->getG())
            return true;
        else
            return false;

    }
};

#endif // COMPARISONUNIFORMCOST_H
